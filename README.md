# Songbook

https://zpevnik.skorepova.info/

## How to develop frontend

```
npm start
node proxy.js
```

## How to develop backend

```
yarn global add now
now login
# you might need to link using `now`, you can cancel deploy once it creates .now folder
now env pull
now dev
```

## How to deploy

just push to master
